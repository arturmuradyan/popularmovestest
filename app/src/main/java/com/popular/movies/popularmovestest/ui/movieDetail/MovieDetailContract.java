package com.popular.movies.popularmovestest.ui.movieDetail;

import com.popular.movies.popularmovestest.ui.base.BaseView;
import com.popular.movies.popularmovestest.ui.entities.ResponseMovieDetails;
import com.popular.movies.popularmovestest.ui.entities.ResponseMoviePopular;
import com.popular.movies.popularmovestest.ui.presentation.PresenterLifecycle;

public interface MovieDetailContract {

    interface View  extends BaseView<Presenter> {
        void initViews(android.view.View view);
        void initToolBar(android.view.View view);
        void initRequestParams();
        void updateData(ResponseMovieDetails responseMovieDetails);
    }

    interface Presenter extends PresenterLifecycle<View> {
        void requestMovieDetail(int movie_id, String api_key, String language);
    }

}
