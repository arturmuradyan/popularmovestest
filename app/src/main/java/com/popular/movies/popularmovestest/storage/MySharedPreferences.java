package com.popular.movies.popularmovestest.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.popular.movies.popularmovestest.constants.ConstantsSharedPreferences;

public class MySharedPreferences {

    public static void putString(Context context, String key, String value) {
        if (context == null) return;
        SharedPreferences sPref = context.getSharedPreferences(ConstantsSharedPreferences.PREFERENCES_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(key, value);
        ed.apply();
    }

    public static String getString(Context context, String key, String defValue) {
        SharedPreferences sPref = context.getSharedPreferences(ConstantsSharedPreferences.PREFERENCES_TAG, Context.MODE_PRIVATE);
        return sPref.getString(key, defValue);
    }
    public static void putObject(Context context, String objectName, Object object) {
        putString(context, objectName, new Gson().toJson(object));
    }

    public static Object getObject(Context context, String objectName, Class classObject) {
        return new Gson().fromJson(getString(context, objectName, null) , classObject);
    }

    public static boolean clear(Context context) {
        if (context == null){
            return false;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences(ConstantsSharedPreferences.PREFERENCES_TAG, 0);
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        return mEditor.clear().commit();
    }

}
