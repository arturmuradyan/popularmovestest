package com.popular.movies.popularmovestest.ui.presentation;

public interface PresenterLifecycle<T> {

    /**
     * Binds presenter with a view when created.
     *
     * @param view the view associated with this presenter
     */

    void onCreate(T view);

    /**
     * Drops the reference to the view when destroyed
     */
    void onDestroy();

    /**
     * Drops the reference to the view when resumed
     */
    void onResume();

}
