package com.popular.movies.popularmovestest.ui.movieDetail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.popular.movies.popularmovestest.R;
import com.popular.movies.popularmovestest.network.NetworkModule;
import com.popular.movies.popularmovestest.ui.entities.ResponseMovieDetails;
import com.popular.movies.popularmovestest.ui.entities.Result;
import com.popular.movies.popularmovestest.util.Utils;

import androidx.fragment.app.Fragment;


public class MovieDetailFragment extends Fragment implements MovieDetailContract.View {

    private MovieDetailPresenter mMovieDetailPresenter;

    private String moreInfoURL;
    private TextView title;
    private TextView description;
    private TextView genres;
    private TextView genre;
    private TextView revenue;
    private TextView budget;
    private TextView moreInfo;
    private ImageView image;

    private Result movie;

    public MovieDetailFragment(Result movie) {
        this.movie = movie;
    }

    public MovieDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);

        mMovieDetailPresenter = new MovieDetailPresenter(getActivity());
        mMovieDetailPresenter.onCreate(this);

        initViews(view);
        initToolBar(view);
        initRequestParams();

        return view;
    }

    @Override
    public void initViews(View view) {
        title = view.findViewById(R.id.title);
        description = view.findViewById(R.id.description);
        genre = view.findViewById(R.id.genre);
        revenue = view.findViewById(R.id.revenue);
        budget = view.findViewById(R.id.budget);
        genres = view.findViewById(R.id.genres);
        image = view.findViewById(R.id.image);
        moreInfo = view.findViewById(R.id.more_info);

        title.setText(movie.getTitle());
        description.setText(movie.getOverview());
        String url = NetworkModule.IMAGE_BASE_URL + movie.getPosterPath();
        String thumbnailUrl = NetworkModule.IMAGE_THUMBNAIL_BASE_URL + movie.getPosterPath();
        Glide.with(getActivity())
                .load(url)
                .thumbnail(Glide.with(getActivity()).load(thumbnailUrl))
                .placeholder(R.drawable.placeholder)
                .into(image);


        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isNetworkConnected(getActivity()) && !moreInfoURL.equals("")) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(moreInfoURL));
                    startActivity(browserIntent);
                } else {
                    Toast.makeText(getActivity(), "Url not found!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void initToolBar(View view) {
        view.findViewById(R.id.tool_bar_back_btn).setVisibility(View.VISIBLE);
        view.findViewById(R.id.tool_bar_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void initRequestParams() {
        String language = "en-US";
        mMovieDetailPresenter.requestMovieDetail(movie.getId(), getActivity().getResources().getString(R.string.api_key), language);
    }

    @Override
    public void updateData(ResponseMovieDetails responseMovieDetails) {
        if (getContext() == null) {
            return;
        }
        budget.setVisibility(View.VISIBLE);
        moreInfo.setVisibility(View.VISIBLE);
        moreInfoURL = responseMovieDetails.getHomepage();

        //Set data of Revenue
        if (responseMovieDetails.getRevenue() != null) {
            revenue.setVisibility(View.VISIBLE);

            revenue.setText(getActivity().getResources().getString(R.string.revenue) + " " + responseMovieDetails.getRevenue() + "$");
        }

        //Set data of Budget
        if (responseMovieDetails.getBudget() != null) {
            genre.setVisibility(View.VISIBLE);
            budget.setText(getActivity().getResources().getString(R.string.budget) + " " + responseMovieDetails.getBudget() + "$");
        }

        //Set data of Genres
        for (int i = 0; i < responseMovieDetails.getGenres().size(); i++) {
            genres.setText(genres.getText() + responseMovieDetails.getGenres().get(i).getName());
            if (i != responseMovieDetails.getGenres().size() - 1) {
                genres.setText(genres.getText() + " / ");
            }
        }

        String url = NetworkModule.IMAGE_BASE_URL + movie.getPosterPath();
        Glide.with(getActivity())
                .load(url)
                .placeholder(R.drawable.placeholder)
                .into(image);

    }

    @Override
    public void onError(String message) {

        if (!Utils.isNetworkConnected(getActivity())) {
            Toast.makeText(getActivity(), "No internet connection!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }

    }
}
