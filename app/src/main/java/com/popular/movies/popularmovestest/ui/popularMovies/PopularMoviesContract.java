package com.popular.movies.popularmovestest.ui.popularMovies;

import com.popular.movies.popularmovestest.ui.base.BaseView;
import com.popular.movies.popularmovestest.ui.entities.ResponseMoviePopular;
import com.popular.movies.popularmovestest.ui.entities.Result;
import com.popular.movies.popularmovestest.ui.presentation.PresenterLifecycle;

import java.util.List;

public interface PopularMoviesContract {

    interface View extends BaseView<Presenter> {
        void initViews(android.view.View view);

        void initMovieRecyclerView(android.view.View view);

        void initRequestParams();

        void onMoviePopularSuccess(ResponseMoviePopular responseMoviePopular);

        List<Result> updateStarsMovies(List<Result> starMovies);
    }

    interface Presenter extends PresenterLifecycle<View> {
        void requestMoviePopular(String api_key, String language, int page);
    }

}
