package com.popular.movies.popularmovestest.ui.base;


import com.popular.movies.popularmovestest.ui.presentation.PresenterLifecycle;

public abstract class BasePresenter<T extends BaseView> implements PresenterLifecycle<T> {

    private T mView;

    @Override
    public void onCreate(T view) {
        mView = view;
    }

    @Override
    public void onDestroy() {
        mView = null;
    }

    @Override
    public void onResume() {
    }

    protected T getView() {
        return mView;
    }
}
