package com.popular.movies.popularmovestest.ui.popularMovies;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.popular.movies.popularmovestest.MainActivity;
import com.popular.movies.popularmovestest.R;
import com.popular.movies.popularmovestest.storage.MySharedPreferences;
import com.popular.movies.popularmovestest.ui.entities.ResponseMoviePopular;
import com.popular.movies.popularmovestest.ui.entities.Result;
import com.popular.movies.popularmovestest.ui.movieDetail.MovieDetailFragment;
import com.popular.movies.popularmovestest.util.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.popular.movies.popularmovestest.constants.ConstantsSharedPreferences.POPULAR_MOVIES_LIST;


public class PopularMoviesFragment extends Fragment implements PopularMoviesContract.View, PopularMoviesAdapter.OnItemClickListener {

    private RecyclerView mPopularMovieRV;
    private PopularMoviesAdapter mPopularMoviesAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private PopularMoviesPresenter mPopularMoviesPresenter;
    private UpdateMoviesStarsListener updateMoviesStarsListener;

    private boolean loading = true;
    private int pastVisibleItems;
    private int visibleItemCount;
    private int totalItemCount;
    private int page = 0;

    private List<Result> starMoviesList;

    public PopularMoviesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_popular_movie, container, false);

        mPopularMoviesPresenter = new PopularMoviesPresenter(getActivity());
        mPopularMoviesPresenter.onCreate(this);

        initMovieRecyclerView(view);
        initViews(view);
        initRequestParams();

        return view;
    }

    @Override
    public void initViews(View view) {

        //Set Popular Movies caching data (first 20 items)
        Type type = new TypeToken<List<Result>>() {
        }.getType();
        String json = MySharedPreferences.getString(getActivity(), POPULAR_MOVIES_LIST, "");
        List<Result> list = (new Gson()).fromJson(json, type);
        if (list != null) {
            mPopularMoviesAdapter.addAll(list);
        }
    }

    @Override
    public void initMovieRecyclerView(View view) {
        mPopularMovieRV = view.findViewById(R.id.mPopular_movie_rv);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mPopularMovieRV.setLayoutManager(mLayoutManager);
        mPopularMoviesAdapter = new PopularMoviesAdapter();
        mPopularMovieRV.setAdapter(mPopularMoviesAdapter);
        mPopularMoviesAdapter.setItemClickListener(this);

        mPopularMovieRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if (loading && (visibleItemCount + pastVisibleItems) >= totalItemCount - 1) {
                        loading = false;
                        initRequestParams();
                    }
                }
            }
        });

    }

    @Override
    public void initRequestParams() {
        //add null , so the adapter will check view_type and show progress bar at bottom
        if (page > 0) {
            mPopularMoviesAdapter.add(null);
            mPopularMoviesAdapter.notifyItemInserted(mPopularMoviesAdapter.getItemCount() - 1);
        }

        String language = "en-US";
        mPopularMoviesPresenter.requestMoviePopular(getActivity().getResources().getString(R.string.api_key), language, ++page);
    }

    @Override
    public void onItemClick(View view, int position) {
        ((MainActivity) getActivity()).pushFragment(new MovieDetailFragment(mPopularMoviesAdapter.getItem(position)), true);
    }

    @Override
    public void onStarClick(Result movie, int position) {
        if (movie.getStar()) {
            updateMoviesStarsListener.insertMovies(movie);
        } else {
            updateMoviesStarsListener.deleteMovies(movie);
        }
    }

    @Override
    public void onMoviePopularSuccess(ResponseMoviePopular responseMoviePopular) {
        //   remove progress item
        mPopularMoviesAdapter.remove(mPopularMoviesAdapter.getItemCount() - 1);
        mPopularMoviesAdapter.notifyItemRemoved(mPopularMoviesAdapter.getItemCount());

        List<Result> list = updateStarsMovies(responseMoviePopular.getResults());
        loading = true;
        if (page == 1) {
            mPopularMoviesAdapter.setData(list);
        } else {
            mPopularMoviesAdapter.addAll(list);
        }
    }

    @Override
    public List<Result> updateStarsMovies(List<Result> starMovies) {
        if (starMoviesList == null || starMoviesList.size() == 0) {
            return starMovies;
        }
        for (int i = 0; i < starMovies.size(); i++) {
            for (int j = 0; j < starMoviesList.size(); j++) {
                if (starMovies.get(i).getId().equals(starMoviesList.get(j).getId())) {
                    starMovies.get(i).setStar(true);
                    continue;
                }
            }
        }
        return starMovies;
    }

    public void updateItem(Result movie) {
        for (int i = 0; i < mPopularMoviesAdapter.getItemCount(); i++) {
            if (movie.getId().equals(mPopularMoviesAdapter.getItem(i).getId())) {
                mPopularMoviesAdapter.getItem(i).setStar(false);
                mPopularMoviesAdapter.notifyItemChanged(i);
                break;
            }
        }
    }

    public void setSelectedMovieList(List<Result> movies) {
        starMoviesList = new ArrayList<>(movies);
    }

    @Override
    public void onError(String message) {
        mPopularMoviesAdapter.remove(mPopularMoviesAdapter.getItemCount() - 1);
        mPopularMoviesAdapter.notifyItemRemoved(mPopularMoviesAdapter.getItemCount());

        if (!Utils.isNetworkConnected(getActivity())) {
            Toast.makeText(getActivity(), "No internet connection!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }

    }

    public interface UpdateMoviesStarsListener {
        void insertMovies(Result movie);

        void deleteMovies(Result movie);
    }

    public void setUpdateMoviesStarsListener(UpdateMoviesStarsListener updateMoviesStarsListener) {
        this.updateMoviesStarsListener = updateMoviesStarsListener;
    }
}
