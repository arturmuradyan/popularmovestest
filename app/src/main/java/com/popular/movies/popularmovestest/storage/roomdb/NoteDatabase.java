package com.popular.movies.popularmovestest.storage.roomdb;

import com.popular.movies.popularmovestest.storage.roomDao.DaoAccess;
import com.popular.movies.popularmovestest.ui.entities.Result;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Result.class}, version = 1, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {

    public abstract DaoAccess daoAccess();
}
