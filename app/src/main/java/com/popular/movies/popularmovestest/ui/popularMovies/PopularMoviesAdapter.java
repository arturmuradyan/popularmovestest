package com.popular.movies.popularmovestest.ui.popularMovies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.popular.movies.popularmovestest.R;
import com.popular.movies.popularmovestest.network.NetworkModule;
import com.popular.movies.popularmovestest.ui.entities.Result;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class PopularMoviesAdapter extends RecyclerView.Adapter {


    private List<Result> mPopularList;
    private OnItemClickListener itemClickListener;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public void setData(List<Result> list) {
        if (list == null) {
            list = new ArrayList<>();
        }
        mPopularList = list;
        notifyDataSetChanged();
    }

    public void addAll(List<Result> itemList) {
        if (mPopularList == null) {
            mPopularList = new ArrayList<>();
        }
        mPopularList.addAll(itemList);
        notifyDataSetChanged();
    }

    public void add(Result item) {
        mPopularList.add(item);
        notifyItemInserted(mPopularList.size() - 1);
    }

    public void remove(int position) {
        if (this.mPopularList == null || (position > getItemCount() - 1) || position == -1) {
            return;
        }
        mPopularList.remove(position);
    }


    public PopularMoviesAdapter() {
        if (this.mPopularList == null) {
            this.mPopularList = new ArrayList<>();
        }
    }

    public Result getItem(int position) {
        return mPopularList.get(position);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_popular_movie, parent, false);

            vh = new PopularMovieViewHolder(v, parent.getContext());
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PopularMovieViewHolder) {
            ((PopularMovieViewHolder) holder).bind(mPopularList.get(position), position);
        } else {
//            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return mPopularList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public int getItemCount() {
        if (mPopularList != null) {
            return mPopularList.size();
        }
        return 0;
    }

    class PopularMovieViewHolder extends RecyclerView.ViewHolder {

        Context context;
        TextView title;
        TextView description;
        ImageView image1;
        ImageView star;

        public PopularMovieViewHolder(@NonNull View itemView, final Context context) {
            super(itemView);
            this.context = context;

            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            image1 = itemView.findViewById(R.id.image1);
            star = itemView.findViewById(R.id.star);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(v, getAdapterPosition());
                }
            });

            star.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        mPopularList.get(getAdapterPosition()).setStar(!mPopularList.get(getAdapterPosition()).getStar());
                        itemClickListener.onStarClick(mPopularList.get(getAdapterPosition()), getAdapterPosition());
                        notifyItemChanged(getAdapterPosition());
                    }

                }
            });

        }

        void bind(final Result popularItem, int position) {
            title.setText(popularItem.getTitle());
            description.setText(popularItem.getOverview());

            String url = NetworkModule.IMAGE_BASE_URL + popularItem.getBackdropPath();
            String thumbnailUrl = NetworkModule.IMAGE_THUMBNAIL_BASE_URL + popularItem.getBackdropPath();

            Glide.with(context.getApplicationContext())
                    .load(url)
                    .thumbnail(Glide.with(context).load(thumbnailUrl))
                    .placeholder(R.drawable.placeholder)
                    .into(image1);

            if (mPopularList.get(position).getStar()) {
                star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_selected));
            } else {
                star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_unselected));
            }

        }
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressViewHolder(View v) {
            super(v);
        }
    }

    interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onStarClick(Result movie, int position);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}



