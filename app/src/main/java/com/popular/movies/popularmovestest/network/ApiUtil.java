package com.popular.movies.popularmovestest.network;

public class ApiUtil {

    public static ApiService request(){
        return NetworkModule.getRetrofit().create(ApiService.class);
    }
}
