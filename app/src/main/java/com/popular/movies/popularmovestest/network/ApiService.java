package com.popular.movies.popularmovestest.network;

import com.popular.movies.popularmovestest.ui.entities.ResponseMovieDetails;
import com.popular.movies.popularmovestest.ui.entities.ResponseMoviePopular;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/3/movie/popular")
    Call<ResponseMoviePopular> requestMoviePopular(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int page);

    @GET("/3/movie/{movie_id}")
    Call<ResponseMovieDetails> requestMovieDetail(@Path("movie_id") int movie_id, @Query("api_key") String api_key, @Query("language") String language);
}
