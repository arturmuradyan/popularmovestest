package com.popular.movies.popularmovestest.ui.base;

public interface BaseView<T> {

    void onError(String message);
}
