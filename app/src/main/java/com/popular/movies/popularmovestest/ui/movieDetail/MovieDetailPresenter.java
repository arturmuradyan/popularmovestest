package com.popular.movies.popularmovestest.ui.movieDetail;


import android.content.Context;
import android.util.Log;

import com.popular.movies.popularmovestest.MainActivity;
import com.popular.movies.popularmovestest.network.ApiUtil;
import com.popular.movies.popularmovestest.ui.base.BasePresenter;
import com.popular.movies.popularmovestest.ui.entities.ResponseMovieDetails;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class MovieDetailPresenter extends BasePresenter<MovieDetailContract.View> implements MovieDetailContract.Presenter {

    private Context context;

    public MovieDetailPresenter(Context context) {
        this.context = context;
    }


    @Override
    public void requestMovieDetail(int movie_id, String api_key, String language) {
        ((MainActivity) context).showLoader();
        ApiUtil.request().requestMovieDetail(movie_id, api_key, language).enqueue(new Callback<ResponseMovieDetails>() {
            @Override
            public void onResponse(Call<ResponseMovieDetails> call, Response<ResponseMovieDetails> response) {
                ResponseMovieDetails responseMovieDetails = response.body();
                if (responseMovieDetails != null) {
                    getView().updateData(responseMovieDetails);
                } else {
                    getView().onError(response.message());
                }
                ((MainActivity) context).hideLoader();

            }

            @Override
            public void onFailure(Call<ResponseMovieDetails> call, Throwable t) {
                //showErrorMessage();
                Log.e(TAG, "error loading from API " + t.getMessage());
                getView().onError(t.getMessage());
                ((MainActivity) context).hideLoader();
            }

        });
    }
}
