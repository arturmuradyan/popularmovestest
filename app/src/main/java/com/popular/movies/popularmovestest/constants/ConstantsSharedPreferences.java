package com.popular.movies.popularmovestest.constants;

public final class ConstantsSharedPreferences {

    public static final String PREFERENCES_TAG = "preferences_tag";
    public static final String POPULAR_MOVIES_LIST = "popular_movies_list";

}