package com.popular.movies.popularmovestest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

import com.popular.movies.popularmovestest.ui.home.HomeTabFragment;
import com.popular.movies.popularmovestest.ui.popularMovies.PopularMoviesFragment;

public class MainActivity extends AppCompatActivity {

    private View loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loader = findViewById(R.id.loader);
        pushFragment(new HomeTabFragment(), false);
    }

    public void pushFragment(Fragment fragment, boolean addBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if (addBackStack) {
            transaction.add(R.id.frame_layout, fragment, fragment.getClass().getSimpleName());
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        } else {
            transaction.replace(R.id.frame_layout, fragment);
        }
        transaction.commit();
    }

    public void showLoader() {
        loader.setVisibility(View.VISIBLE);
    }

    public void hideLoader() {
        loader.setVisibility(View.GONE);
    }

}
