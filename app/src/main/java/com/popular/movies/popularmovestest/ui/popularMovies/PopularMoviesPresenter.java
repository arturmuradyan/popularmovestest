package com.popular.movies.popularmovestest.ui.popularMovies;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.popular.movies.popularmovestest.MainActivity;
import com.popular.movies.popularmovestest.network.ApiUtil;
import com.popular.movies.popularmovestest.storage.MySharedPreferences;
import com.popular.movies.popularmovestest.ui.base.BasePresenter;
import com.popular.movies.popularmovestest.ui.entities.ResponseMoviePopular;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.popular.movies.popularmovestest.constants.ConstantsSharedPreferences.POPULAR_MOVIES_LIST;


public class PopularMoviesPresenter extends BasePresenter<PopularMoviesContract.View> implements PopularMoviesContract.Presenter {

    private Context context;

    public PopularMoviesPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void requestMoviePopular(String api_key, String language, final int page) {
        if (page == 1) {
            ((MainActivity) context).showLoader();
        }
        ApiUtil.request().requestMoviePopular(api_key, language, page).enqueue(new Callback<ResponseMoviePopular>() {
            @Override
            public void onResponse(Call<ResponseMoviePopular> call, Response<ResponseMoviePopular> response) {
                ResponseMoviePopular responseMoviePopular = response.body();
                if (responseMoviePopular != null) {

                    //Saved popular movies first response list in SharedPreferences
                    if (page == 1) {
                        String jsonMovies = (new Gson()).toJson(responseMoviePopular.getResults());
                        MySharedPreferences.putString(context, POPULAR_MOVIES_LIST, jsonMovies);
                    }

                    getView().onMoviePopularSuccess(responseMoviePopular);
                } else {
                    getView().onError(response.message());
                }
                ((MainActivity) context).hideLoader();

            }

            @Override
            public void onFailure(Call<ResponseMoviePopular> call, Throwable t) {
                //showErrorMessage();
                Log.e(TAG, "error loading from API " + t.getMessage());
                getView().onError(t.getMessage());
                ((MainActivity) context).hideLoader();
            }

        });
    }
}
