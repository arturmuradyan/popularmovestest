package com.popular.movies.popularmovestest.storage.roomDao;


import com.popular.movies.popularmovestest.ui.entities.Result;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface DaoAccess {

    @Insert
    Long insertTask(Result movie);


    @Query("SELECT * FROM Result")
    LiveData<List<Result>> fetchAllTasks();


    @Query("SELECT * FROM Result WHERE id =:movieId")
    LiveData<Result> getMovie(int movieId);


    @Delete
    void deleteTask(Result movie);
}
