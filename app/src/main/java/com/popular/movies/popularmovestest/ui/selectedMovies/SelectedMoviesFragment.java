package com.popular.movies.popularmovestest.ui.selectedMovies;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.popular.movies.popularmovestest.MainActivity;
import com.popular.movies.popularmovestest.R;
import com.popular.movies.popularmovestest.ui.entities.Result;
import com.popular.movies.popularmovestest.ui.movieDetail.MovieDetailFragment;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectedMoviesFragment extends Fragment implements SelectedMoviesAdapter.OnItemClickListener {

    private RecyclerView mPopularMovieRV;
    private SelectedMoviesAdapter mSelectedMoviesAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView noSelectedItems;
    private UpdateSelectedMoviesListener updateSelectedMoviesListener;

    public SelectedMoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_selected_movies, container, false);

        initView(view);
        return view;
    }

    public void initView(View view) {
        mPopularMovieRV = view.findViewById(R.id.mPopular_movie_rv);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mPopularMovieRV.setLayoutManager(mLayoutManager);
        mSelectedMoviesAdapter = new SelectedMoviesAdapter();
        mPopularMovieRV.setAdapter(mSelectedMoviesAdapter);
        mSelectedMoviesAdapter.setItemClickListener(this);

        noSelectedItems = view.findViewById(R.id.no_items);
    }

    @Override
    public void onItemClick(View view, int position) {
        ((MainActivity) getActivity()).pushFragment(new MovieDetailFragment(mSelectedMoviesAdapter.getItem(position)), true);
    }

    @Override
    public void onStarClick(Result movie, int position) {
        mSelectedMoviesAdapter.remove(position);
        updateSelectedMoviesListener.unselectMovies(movie);

        if (mSelectedMoviesAdapter.getItemCount() == 0) {
            noSelectedItems.setVisibility(View.VISIBLE);
        }
    }

    public void setSelectedMovies(List<Result> movies) {
        if (movies == null)
            return;

        if (movies.size() == 0) {
            noSelectedItems.setVisibility(View.VISIBLE);
            mSelectedMoviesAdapter.clearItems();
        } else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    noSelectedItems.setVisibility(View.GONE);
                }
            });
            mSelectedMoviesAdapter.setData(movies);
        }
    }

    public interface UpdateSelectedMoviesListener {
        void unselectMovies(Result movie);
    }

    public void setUpdateSelectedMoviesListener(UpdateSelectedMoviesListener updateSelectedMoviesListener) {
        this.updateSelectedMoviesListener = updateSelectedMoviesListener;
    }
}
