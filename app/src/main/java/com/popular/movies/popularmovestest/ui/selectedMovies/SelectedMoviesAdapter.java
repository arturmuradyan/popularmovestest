package com.popular.movies.popularmovestest.ui.selectedMovies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.popular.movies.popularmovestest.R;
import com.popular.movies.popularmovestest.network.NetworkModule;
import com.popular.movies.popularmovestest.ui.entities.Result;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class SelectedMoviesAdapter extends RecyclerView.Adapter<SelectedMoviesAdapter.SelectedMovieViewHolder> {


    private List<Result> mPopularList;
    private OnItemClickListener itemClickListener;

    public void setData(List<Result> list) {
        mPopularList = list;
        notifyDataSetChanged();
    }

    public void remove(int position) {
        if (this.mPopularList == null || (position > getItemCount() - 1) || position == -1) {
            return;
        }
        notifyItemRemoved(position);
        mPopularList.remove(position);
    }


    public SelectedMoviesAdapter() {
        if (this.mPopularList == null) {
            this.mPopularList = new ArrayList<>();
        }
    }

    public Result getItem(int position) {
        return mPopularList.get(position);
    }


    @Override
    public SelectedMovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_movies, parent, false);
        return new SelectedMovieViewHolder(view, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedMovieViewHolder holder, int position) {
        holder.bind(mPopularList.get(position), position);
    }

    @Override
    public int getItemCount() {
        if (mPopularList != null) {
            return mPopularList.size();
        }
        return 0;
    }

    public void clearItems() {
        mPopularList.clear();
        notifyDataSetChanged();
    }

    class SelectedMovieViewHolder extends RecyclerView.ViewHolder {

        Context context;
        TextView title;
        TextView description;
        ImageView image1;
        ImageView star;

        public SelectedMovieViewHolder(@NonNull View itemView, final Context context) {
            super(itemView);
            this.context = context;

            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            image1 = itemView.findViewById(R.id.image1);
            star = itemView.findViewById(R.id.star);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(v, getAdapterPosition());
                }
            });

            star.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        mPopularList.get(getAdapterPosition()).setStar(!mPopularList.get(getAdapterPosition()).getStar());
                        itemClickListener.onStarClick(mPopularList.get(getAdapterPosition()), getAdapterPosition());
                        notifyItemChanged(getAdapterPosition());
                    }

                }
            });

        }

        void bind(final Result popularItem, int position) {
            title.setText(popularItem.getTitle());
            description.setText(popularItem.getOverview());

            String url = NetworkModule.IMAGE_BASE_URL + popularItem.getBackdropPath();
            String thumbnailUrl = NetworkModule.IMAGE_THUMBNAIL_BASE_URL + popularItem.getBackdropPath();

            Glide.with(context.getApplicationContext())
                    .load(url)
                    .thumbnail(Glide.with(context).load(thumbnailUrl))
                    .placeholder(R.drawable.placeholder)
                    .into(image1);

            if (mPopularList.get(position).getStar()) {
                star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_selected));
            } else {
                star.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_star_unselected));
            }

        }
    }


    interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onStarClick(Result movie, int position);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}



