package com.popular.movies.popularmovestest.storage.repository;

import android.content.Context;
import android.os.AsyncTask;

import com.popular.movies.popularmovestest.storage.roomdb.NoteDatabase;
import com.popular.movies.popularmovestest.ui.entities.Result;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Room;


public class ItemRepository {

    private String DB_NAME = "movie_db";

    private NoteDatabase noteDatabase;
    public ItemRepository(Context context) {
        noteDatabase = Room.databaseBuilder(context, NoteDatabase.class, DB_NAME).build();
    }

    public void insertTask(final Result movie) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().insertTask(movie);
                return null;
            }
        }.execute();
    }

    public void deleteTask(final Result movie) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                noteDatabase.daoAccess().deleteTask(movie);
                return null;
            }
        }.execute();
    }

    public LiveData<List<Result>> getTasks() {
        return noteDatabase.daoAccess().fetchAllTasks();
    }
}
