package com.popular.movies.popularmovestest.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.popular.movies.popularmovestest.R;
import com.popular.movies.popularmovestest.storage.repository.ItemRepository;
import com.popular.movies.popularmovestest.ui.entities.Result;
import com.popular.movies.popularmovestest.ui.popularMovies.PopularMoviesFragment;
import com.popular.movies.popularmovestest.ui.selectedMovies.SelectedMoviesFragment;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;

public class HomeTabFragment extends Fragment implements PopularMoviesFragment.UpdateMoviesStarsListener, SelectedMoviesFragment.UpdateSelectedMoviesListener {

    private HomeViewPagerAdapter adapter;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private PopularMoviesFragment popularMoviesFragment;
    private SelectedMoviesFragment selectedMoviesFragment;

    private ItemRepository itemRepository;

    public HomeTabFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_tab, container, false);

        initViews(view);

        return view;
    }

    private void initViews(View view) {

        popularMoviesFragment = new PopularMoviesFragment();
        selectedMoviesFragment = new SelectedMoviesFragment();

        mViewPager = view.findViewById(R.id.fragment_activate_viewpager);
        mTabLayout = view.findViewById(R.id.fragment_activate_tablayout);

        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabItems();

        itemRepository = new ItemRepository(getActivity());
        popularMoviesFragment.setUpdateMoviesStarsListener(this);
        selectedMoviesFragment.setUpdateSelectedMoviesListener(this);
        updateSelectedList();

    }

    private void setupTabItems() {
        for (int i = 0; i < adapter.getCount(); i++) {

            View headerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.tab_custom_item, null, false);

            ConstraintLayout constraintLayout = headerView.findViewById(R.id.custom_tab_layout_root);
            TextView title = headerView.findViewById(R.id.custom_tab_title);

            title.setText(adapter.getPageTitle(i));

            TabLayout.Tab tab = mTabLayout.getTabAt(i).setCustomView(constraintLayout);
            if (tab != null) {
                tab.setCustomView(constraintLayout);
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new HomeViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(popularMoviesFragment, getResources().getString(R.string.popular_movies));
        adapter.addFragment(selectedMoviesFragment, getResources().getString(R.string.selected_movies));
        viewPager.setAdapter(adapter);
    }

    private void updateSelectedList() {
        itemRepository.getTasks().observe(this, new Observer<List<Result>>() {
            @Override
            public void onChanged(@Nullable List<Result> starMovies) {
                    selectedMoviesFragment.setSelectedMovies(starMovies);
                    popularMoviesFragment.setSelectedMovieList(starMovies);
            }
        });
    }

    @Override
    public void insertMovies(Result movie) {
        itemRepository.insertTask(movie);
    }

    @Override
    public void deleteMovies(Result movie) {
        itemRepository.deleteTask(movie);
    }

    @Override
    public void unselectMovies(Result movie) {
        itemRepository.deleteTask(movie);
        popularMoviesFragment.updateItem(movie);
    }
}
